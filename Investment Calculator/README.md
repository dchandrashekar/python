# Investment Calculator
# Background
Financial mathematics
The monthly growth can be calculated from the annual one by dividing the annual growth evenly for each month. This can be calculated using the formula monthly_profit_multiplier = (1 + annual_profit_multiplier) ** (1 / 12) - 1 - this is already implemented for you.

Let's for instance assume the annual growth/profit is 8 %. This means that if the value of the investment is x at the beginning of the year, it is x + 0.08 * x ie. 1.08 * x at the end of the year (if we ignore the additional investments / withdrawals).

The monthly profit multiplier in this case is some value m for which m ** 12 == 1.08. Thic can be calculated as m = 1.08 ** (1 / 12) ie. by taking the 12th root of the annual growth multiplier. In our example case the value of m is about 1.0064 which signifying the monthly growth of 0.64 %. Now we can easily calculate the monthly profit. f the balance is y at the beginning of the month, it is y + m * y at the end of it. For instance 1000 euros would become 1006.4 euros with this growth rate.

In addition to the absolute balance, we want to examine the profits themselves as well, so we are using profit multipliers. In this case, the formula is otherwise the same except, due to adding and substracting 1's, it gets the form m = (1 + a) ** (1 / 12) - 1 where a is the annual growth rate (eg. 8% ie. 0.08) and m is the monthly one (eg. 0.64% ie. 0.064).

The actual profit depends on the timing of the additional investent / withdrawal. In this exercise we assume the monthly investment / withdrawal to be made at the very end of the month. This means it is added to / substracted from the balance after it has already grown due to the interest.

Cumulative profit means the total profit at that point.

# Description 
The program asks the user for the essential values
The program prints a table of the monthly returns. For each month, the program prints
Numbers of the month and the year in question
The balance at the beginning of the month ("Start balance")
The earned profit/return during the month ("Monthly profit")
The balance at the end of the month ("End balance")
The total profit cumulated during the whole investment at the end of the month ("Cumulative profit")
The program continues printing the table until
The investment time given by the user is fulfilled. (If it would be exceeded, the exceeding part is not printed. For instance 3.8 years is exactly 3 vuotta and 9.6 months, which means we would print 3 years and 9 months of the table.)
or
The balance would go below zero (this can occur only if the user gave a negative value to the variable per_month_investment, which that case means the amount they are withdrawing from their investment each month.
The program prints the final balance, final total profits and the net deposit amount according the the example runs.

# Execution
Run the investment_calc.py file "python investment_calc.py"

Note: No errors are handled