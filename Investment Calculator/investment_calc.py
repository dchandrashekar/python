def main():
    print("This program calculates and summarizes the estimated profits from an investment.")

    initial_investment    = float(input("Initial investment sum (eur):\n"))
    annual_profit_percent = float(input("Expected annual growth / return rate (including expenses) (%):\n"))
    per_month_investment  = float(input("Monthly investment (+) or withdrawal (-) (eur):\n"))
    years                 = float(input("For how many years are you planning to hold the investment?\n"))

    e = "eur"
    print('{:>5s} | {:>5s} | {:^10s} | {:^10s} | {:^10s} | {:^10s}'.format('Year','Month','Start','Monthly', 'End', 'Cumulative'))
    print('{:>5s} | {:>5s} | {:^10s} | {:^10s} | {:^10s} | {:^10s}'.format('','','balance','Profit', 'balance','Profit'))
    print('{:>5s} | {:>5s} | {:>10s} | {:>10s} | {:>10s} | {:>10s}'.format('','',e,e,e,e))
    print('-' * 65)

    annual_profit_multiplier = 0.01 * annual_profit_percent
    # There are 12 months in a year; let's divide the exponential growth to them expecting it to be constant
    monthly_profit_multiplier = (1 + annual_profit_multiplier) ** (1 / 12) - 1

    # Write your code here
    start_balance = initial_investment
    yrs = int(years)
    mon = int((years % 1) * 12)
    count = 0
    for y in range(1,yrs+1):
        for m in range(1,12+1):
            count += 1
            monthly_profit = start_balance * monthly_profit_multiplier
            end_balance = start_balance + monthly_profit + per_month_investment
            if end_balance > 0:
                cumulative_profit = end_balance - initial_investment - (per_month_investment * count)
                print("{:5d} | {:5d} | {:10.2f} | {:10.2f} | {:10.2f} | {:10.2f}".format(y, m, start_balance, monthly_profit, end_balance, cumulative_profit))
                start_balance = end_balance
                net_deposit = end_balance
            elif end_balance < 0:
                break
        if end_balance > 0:
            print('-' * 65)
    mcount = 0
    if mon > 0:
        for n in range(1,mon+1):
            mcount += 1
            monthly_profit = start_balance * monthly_profit_multiplier
            end_balance = start_balance + monthly_profit + per_month_investment
            if end_balance > 0:
                cumulative_profit = end_balance - initial_investment - (per_month_investment * count) - (per_month_investment * mcount)
                print("{:5d} | {:5d} | {:10.2f} | {:10.2f} | {:10.2f} | {:10.2f}".format((yrs+1), n, start_balance, monthly_profit, end_balance, cumulative_profit))
                start_balance = end_balance
                net_deposit = end_balance
            elif end_balance < 0:
                break
    if end_balance < 0:
        print("\nStopped printing as balance cannot go negative.")
    if end_balance > 0:
        print("\n{:<20s}{:>10.2f} eur".format('End balance:',end_balance))
    elif end_balance < 0:
        print("{:<20s}{:>10.2f} eur".format('End balance:',net_deposit))
    print("{:<20s}{:>10.2f} eur".format('Total profit:',cumulative_profit))
    if end_balance > 0:
        print("{:<20s}{:>10.2f} eur".format('Total net deposit:',end_balance - cumulative_profit))
    elif end_balance < 0:
        print("{:<20s}{:>10.2f} eur".format('Total net deposit:',net_deposit - cumulative_profit))

main()