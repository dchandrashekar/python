# Caffine Calculator
# Background
Caffeine is a stimulating substance that can be found, among others, from coffee and tea. In this exercise we calculate and follow an adult's caffeine consumption. We simplify and assume that a coffee cup (of 2.3 dl) contains 120 milligrams of caffeine and a tea cup contains 40 milligrams. In reality, the amount of caffeine depends on the properties of the coffee and for instance whether the tea is green or black. We use the recommendation of consuming at most 400 milligrams of caffeine per day and a rough limit of a lethal daily intake (10 grams).

# Initial preparations

CAFFEINE_IN_COFFEE_CUP =   120 # mg, milligrams
CAFFEINE_IN_TEA_CUP    =    40 # mg
MAX_RECOMMENDED_INTAKE =   400 # mg
LETHAL_DOSE            = 10000 # mg

# Description of the exercise
Program asks the user for the number of cups of coffee and tea they have consumed each day, calculates statistics about their caffeine consumption and prints information about the consumption relative to the recommendations.

First the program asks the user for the number of days they wish to input. Then the program asks the number of coffee and tea cups for that many days. Both are asked until the user enters a positive value. At the beginning of each day the day number is also printed for the user.

The program uses these to calculate and record

The total amount of consumed caffeine (mg), will be later used to calculate the average daily consumption. Here you can utilize the constants CAFFEINE_IN_COFFEE_CUP and CAFFEINE_IN_TEA_CUP.
The number of days where the recommended maximum intake of 400 mg, ie. the constant MAX_RECOMMENDED_INTAKE was exceeded. (Exactly 400 mg is not yet considered an exceeding.)
The number of days where the lethal dose of 10g ie. 10 000 mg, ie. the constant LETHAL_DOSE was exceeded.
In this case, the program also prints the text ** Lethal dose exceeded!! ** right after asking the tea cups.
Finally, the program calculates and prints the daily average caffeine consumption as well as the number of days where the recommended maxium intake was exceeded. In case the user exceeded the lethal dose during some day, information about how many times this happened is also printed.

In case the user entered 0 as the number of days, the program instead prints No consumption data entered.

# Execution
Run the caffine_calc.py file "python caffine_calc.py"
