def main():
    CAFFEINE_IN_COFFEE_CUP =   120 # mg, milligrams
    CAFFEINE_IN_TEA_CUP    =    40 # mg
    MAX_RECOMMENDED_INTAKE =   400 # mg
    LETHAL_DOSE            = 10000 # mg
    print("The program calculates an adult's caffeine intake and gives feedback of the consumption.")
    num_of_days = int(input("How many days you wish to input?\n"))
    if num_of_days == 0:
        print("No consumption data entered.")
    else:
        i = 0
        sum = 0
        count = 0
        lcount = 0
        while i < num_of_days:
            print("Day", (i + 1))
            numofcoff = int(input("How many cups of coffee did you drink?\n"))
            while numofcoff < 0:
                print("The number must be at least 0!")
                numofcoff = int(input("How many cups of coffee did you drink?\n"))
            numoftea = int(input("And how many cups of tea?\n"))
            while numoftea < 0:
                print("The number must be at least 0!")
                numoftea = int(input("And how many cups of tea?\n"))
            coff = CAFFEINE_IN_COFFEE_CUP * numofcoff
            tea = CAFFEINE_IN_TEA_CUP * numoftea
            caffi = coff + tea
            sum += caffi
            if caffi > MAX_RECOMMENDED_INTAKE:
                count += 1
            if caffi > LETHAL_DOSE:
                print("** Lethal dose exceeded!! **")
                lcount += 1
            print("\n")
            i += 1
        average = sum / num_of_days
        print("On average, you consumed{:5.0f} mg of caffeine per day.".format(average))
        print("You exceeded the maximum recommended amount of 400 mg on{:2d} out of{:2d} days.".format(count, num_of_days))
        if lcount > 0:
            print("You exceeded the lethal dose on{:2d} days.".format(lcount))
    
main()