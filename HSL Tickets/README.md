# Helsinki Regional Transport
# Backround
Helsinki Region Transport has renewed their system at the beginning of the year 2019 by dividing the HSL area into zones. Nowadays you can buy single tickets, season tickets and extension tickets for season tickets.

A single AB-zone ticket and a single BC-zone ticket both cost 2.80 euros. A single ABC-zone ticket costs 4.60 euros.
A season ticket for the AB and BC zones both cost 32.80 euros per month for students. A season ticket fot the ABC-zone costs 59.10 euros per month for students.
Extension tickets can be bought to extend the zone for an existing season ticket for a single trip. A C-zone extension ticket for an AB-season ticket and an A-zone extension ticket for a BC-season ticket both cost 2.50 euros.

# Description
Program asks the user for their number of AB-zone, BC-zone and ABC-zone trips per month. After this the program calculates and tells the user, whether they should buy an ABC-zone season ticket, an AB-zone season ticket and use C-zone extension tickets when needed, a BC-zone season ticket and use A-zone extension tickets when needed or to not get a season ticket and only buy single tickets. The program also calculates the price per month for the cheapest option (this includes for example in the AB-zone season ticket the extra price of the C-zone extension ticket for the BC-zone and ABC-zone trips).

If two options have the exactly same price, the program will suggest a season ticket. If two or more season tickets have the same price, the program will first suggest an ABC-zone season ticket, then an AB-zone season ticket and lastly a BC-zone season ticket.

# Execution
Run the tickets.py file "python tickets.py"

Note: No errors are handled