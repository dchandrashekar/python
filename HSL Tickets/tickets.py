def main():
    AB = 2.80
    BC = 2.80
    ABC = 4.60
    S_AB = 32.80
    S_BC = 32.80
    E_A = 2.50
    E_C = 2.50
    S_ABC = 59.10
    ab = int(input("How many AB-zone trips do you make per month?\n"))
    bc = int(input("How many BC-zone trips do you make per month?\n"))
    abc = int(input("How many ABC-zone trips do you make per month?\n"))
    while ab < 0 or bc < 0 or abc < 0:
        print ("The given values cannot be negative!")
        ab = int(input("How many AB-zone trips do you make per month?\n"))
        bc = int(input("How many BC-zone trips do you make per month?\n"))
        abc = int(input("How many ABC-zone trips do you make per month?\n"))
    # To calculate single zone ticket cost
    single_zone_cost = (ab*AB) + (bc*BC) + (abc*ABC)
    # To calculate seasonal AB and BC with zone extension of C or A 
    seasonal_ab = S_AB + (E_C * (bc + abc))
    seasonal_bc = S_BC + (E_A * (ab + abc))

    if single_zone_cost == seasonal_ab == seasonal_bc == S_ABC:
        print("You should buy an ABC-zone season ticket.")
        print("The price per month will be", S_ABC, "euros.")
    elif single_zone_cost < seasonal_ab and single_zone_cost < seasonal_bc and single_zone_cost < S_ABC:
        print("You should buy single tickets for each trip.")
        print("The price per month will be", single_zone_cost, "euros.")
    elif seasonal_ab < single_zone_cost and seasonal_ab < seasonal_bc and seasonal_ab < S_ABC:
        print("You should buy an AB-zone season ticket and C-zone extension tickets when needed.")
        print("The price per month will be", seasonal_ab, "euros.")
    elif seasonal_bc < single_zone_cost and seasonal_bc < seasonal_ab and seasonal_bc < S_ABC:
        print("You should buy a BC-zone season ticket and A-zone extension tickets when needed.")
        print("The price per month will be", seasonal_bc, "euros.")
    elif single_zone_cost == seasonal_ab or single_zone_cost == seasonal_bc or single_zone_cost == S_ABC:
        if single_zone_cost == seasonal_ab:
            print("You should buy an AB-zone season ticket and C-zone extension tickets when needed.")
            print("The price per month will be", seasonal_ab, "euros.")
        if single_zone_cost == seasonal_bc:
            print("You should buy a BC-zone season ticket and A-zone extension tickets when needed.")
            print("The price per month will be", seasonal_bc, "euros.")
        if single_zone_cost == S_ABC:
            print("You should buy an ABC-zone season ticket.")
            print("The price per month will be", S_ABC, "euros.")
    else:
        print("You should buy an ABC-zone season ticket.")
        print("The price per month will be", S_ABC, "euros.")

main()




