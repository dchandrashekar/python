# Civilization Unit
# Background
This task models the functionality of the military units in the strategy game Civilization V: Brave New World. However, we will make simplifications. In Civilization V, there are 2 types of military units: melee units and ranged units. Each unit has an attack strength and a defense strength. For the melee units, these two are always equal. Each unit also has between 0 and 100 health points (HP). The units can perform the following actions:

Attack against another unit
Ranged units always attack with a ranged attack, which means that only the target of the attack (ie. the defender) suffers damage, not the attacker.
Upon a melee attack, both units suffer damage.
The amount of the damage is specified later on.
Defend from an attack by another unit
The attacking unit always forces the target to defend themselves.
Fortify
Fortifying increases the effective defense strength of the unit (specified later on with detail).
Heal
Healing increases the life points of the unit

# Description of the exercise
In this exercise you do not need to write a main program, but a class defining the properties and the functionality of the civilization unit object.

In the module civilization_unit, create a class CivilizationUnit representing one military unit of Civilization V.

Please note that the name of the module should be written completely in lowercase. Respectively, the name of the class should be written in CamelCase (ie. in the form CivilizationUnit. This is a widely used naming convention which is also required by A+ (or else the grading will not work).

# Description of the CivilizationUnit class
CivilizationUnit has the following class constants:

MAX_HP = 100
DAMAGE_SCALE = 30
RANDOM_DAMAGE_DIFF = 8

MAX_HP is the maximum amount of health points, which is equal for all units.
DAMAGE_SCALE is the expected amount of damage in a situation where 2 units of equal effective strengths fight with each other. All other damage is scaled based on this constant.
RANDOM_DAMAGE_DIFF is the random range of variation, ie. the maximum difference between the expected damage and the actual damage.
CivilizationUnit has the following fields:

__name
name of the unit (a string), for instance "Crossbowman"
__attack_strength
the attack strength of the unit, an integer. Represents the ranged strength for ranged units and melee strength for melee units.
__defense_strength
the defense strength of the unit. For melee units equals to the attack strength.
__fortified
a boolean with a value of True for fortified units and False otherwise.
__ranged
a boolean with a value of True if the unit was defined as a ranged unit upon its initialization and False otherwise and by default.
__hp
The health points of the unit. An integer between 0 and CivilizationUnit.MAX_HP.
Define the following methods for our CivilizationUnit class:
(If the return value is unspecified, the method does not need to return anything.)

__init__(self, unit_name, attack, defense = None, is_ranged = False)
Initializes a new CivilizationUnit object. You may expect all parameters to be valid. New units initially have CivilizationUnit.MAX_HP ie. 100 health points and are not fortified. The other fields are given as parameters.
→ Here defense and is_ranged are so called default parameters (default arguments). Only the first 2 parameters are required, meaning that when creating the object, the user can give between 2 and 4 parameters and the possible remaining ones will automatically be assigned their default values defined in the parameter list, here None and False.

In case only the attack strength was given (ie. if defense == None), the defense strength will automatically be assigned as equal to the attack strength. This way, a melee unit can be created by giving only the first 2 parameters:
    pikeman1 = CivilizationUnit("Pikeman", 16)
    
or equivalently
    pikeman1 = CivilizationUnit(unit_name = "Pikeman", attack = 16)
    
in which case the values of both fields __attack_strength and __defense_strength should be 16 and __ranged should be False.
A ranged unit, however, can be created by giving all 4 parameters:
    bowman1 = CivilizationUnit("Composite Bowman", 11, 7, True)
    
or equivalently
    bowman1 = CivilizationUnit("Composite Bowman", attack = 11, defense = 7, is_ranged = True)
    
in which case all fields are assigned the values given by the user, for instance __defense_strength is 7.

More information about default parameters

get_effective_defense_strength(self)
Calculates and returns the effective defense strength if the unit. It is calculated here with the following simplified formula. The effective defense strength of the unit is 50-100% of its defense strength (ie. the field __defense_strength), scaling linearly based on health points. For instance:
A unit with 100 HP defends with 100% effectiveness
A unit with 50 HP defends with 75 % effectiveness
A unit with 1 HP defends with 50.5 % effectiveness (the lowest amount of life points possible at which the unit is still alive)
A unit with 0 HP would defend with 50 % effectiveness (if it was alive)
Additionally, fortified units defend 40% more effectively than otherwise. This means that the return value of this method is always at least 50% and at most 140% of the unit's __defense_strength.
The method returns the result of this calculation as a float without rounding.
For instance, if an object's __hp field is 60 and __defense is 20, the effectiveness is 80% and the method should therefore return 0.8 * 20 i.e. 16.0 in case __fortified is False or 1.4 * 0.8 * 20 i.e. 22.4 in case __fortified is True.

get_effective_attack_strength(self)
Calculates and returns the effective attack strength, which is determined based on __attack_strength and health points with the same principle as the effective defense strength. However, fortification has no effect on the attack strength.
The method returns the result of this calculation as a float without rounding.
For instance, if an object's __hp field is 60 and __attack is 20, the effectiveness is 80% and the method should therefore return 0.8 * 20 i.e. 16.0 (regardless of __fortified).

is_eliminated(self, other_unit)
Returns True for eliminated units, ie for units with 0 health points. Otherwise returns False.

defend(self, other_unit)
Defends against the attack of other_unit, taking damage that is determined based on the effective defense strength of the defending unit and the effective attack strength of the attacking unit according to the following formula:
excepted_damage_taken = int(CivilizationUnit.DAMAGE_SCALE * other_unit.get_effective_attack_strength() / self.get_effective_defense_strength())

In addition, there is random variation in the actual damage, meaning it can differ from the expected damage at most CivilizationUnit.RANDOM_DAMAGE_DIFF (8 in this exercise) health points to either direction.
In other words, the damage taken by the defending unit is a random integer that is at least excepted_damage_taken - CivilizationUnit.RANDOM_DAMAGE_DIFF and at most excepted_damage_taken + CivilizationUnit.RANDOM_DAMAGE_DIFF, however being always at least 1 (NB: not 0).
In this exercise, the amount of damage must be randomized using random.randint(lower_bound, upper_bound) function where the randomized result is between the integers lower_bound and upper_bound.

The method returns this random value (ie. the damage suffered by the defending unit, ie. self) after substracting it from its life points. The method must ensure that the life points are always at least 0.

attack(self, other_unit)
Attacks against other_unit. First forces the other unit to defend by calling its defend method. (and assigning its return value to some variable). Then the attacking unit (self) suffers damage in the following way:
Ranged units suffer 0 damage when attacking.
Otherwise the unit suffers damage that is calculated in the same principle as for the defending unit, meaning that there is the same random variation equal to at most CivilizationUnit.RANDOM_DAMAGE_DIFF and the health points cannot go below 0. Please note however that you need to adjust the formula to use the correct effective strengths - now other unit is the defending one, and self is the attacking one.)
This does mean that the attacking unit has an advantage, as it deals damage first, possibly reducing the health points and therefore the effective attacking strength of the defending unit, reducing the damage it will deal on the attacking one.
The methods returns 2 values: the damage suffered by the defending unit (ie. the return value of defend that we saved earlier) and the damage suffered by the attacking unit, in this order.

fortify(self)
Fortifies the unit

unfortify(self)
Unfortifies the unit, ie. removes the fortification status from it.

heal(self, amount)
Heals the unit by increasing its health points by amount (integer), ensuring the health points do not exceed CivilizationUnit.MAX_HP (ie. 100).

get_hp(self)
Returns the current health points of the unit, ie. an integer between 0 and CivilizationUnit.MAX_HP.

get_name(self)
Returns the name of the unit (the field __name).

__str__(self)
Return a string representin the specifitacions and status of the unit in the format
[Name] - [Attack]/[Defense] ([Type]) - HP: [Health points]
And adds information about the fortification and elimination status where necessary. (eliminated units are never shown as fortified). Examples of return value strings of the method:
Scout - 5/5 (melee) - HP: 81
Slinger - 7/4 (ranged) - HP: 35, FORTIFIED
Horseman - 12/12 (melee) - HP: 0, ELIMINATED

# Execution
Run the civilization_unit_program.py file "python civilization_unit_program.py"
