# Y1 AUTUMN 2019
# Basic Course in Programming Y1
# Author: Joel Lahenius
# Template for exercise 9.4 CivilizationUnit

import random

class CivilizationUnit:
    
    MAX_HP = 100
    DAMAGE_SCALE = 30
    RANDOM_DAMAGE_DIFF = 8

    def __init__(self, unit_name, attack, defense = None, is_ranged = False):
        "Initializes a new CivilizationUnit"
        self.__name = unit_name
        self.__attack_strength = attack
        if defense == None:
            self.__defense_strength = attack
        else:
            self.__defense_strength = defense
        self.__fortified = False
        self.__ranged = is_ranged
        self.__hp = CivilizationUnit.MAX_HP
    
    def get_name(self):
        return self.__name
    
    def is_alive(self):
        return not self.is_eliminated()
    
    def is_eliminated(self):
        if self.get_hp() == 0:
            return True
        else:
            return False
            
    def get_effective_defense_strength(self):
        old_min = 0; old_max = 100; new_min = 50; new_max = 100
        new_value = ( (self.get_hp() - old_min) / (old_max - old_min) ) * (new_max - new_min) + new_min
        if self.is_fortified():
            effective_defense = 1.4 * (new_value/100) * self.__defense_strength
        else:
            effective_defense = (new_value/100) * self.__defense_strength
        return effective_defense
    
    def get_effective_attack_strength(self):
        old_min = 0; old_max = 100; new_min = 50; new_max = 100
        new_value = ( (self.get_hp() - old_min) / (old_max - old_min) ) * (new_max - new_min) + new_min
        effective_attack = (new_value/100) * self.__attack_strength
        return effective_attack
              
    def attack(self, other_unit):
        damage_dealt = other_unit.defend(self)
        if self.__ranged == True:
            damage_taken = 0
        else:
            damage_taken = self.defend(other_unit)
        return damage_dealt, damage_taken
        
    def defend(self, other_unit):
        excepted_damage_taken = int(CivilizationUnit.DAMAGE_SCALE * other_unit.get_effective_attack_strength() / self.get_effective_defense_strength())
        lower_bound = excepted_damage_taken - CivilizationUnit.RANDOM_DAMAGE_DIFF
        upper_bound = excepted_damage_taken + CivilizationUnit.RANDOM_DAMAGE_DIFF
        var = random.randint(lower_bound, upper_bound)
        if var <= 0:
            damage_taken = 1
        else:
            damage_taken = var
        if (self.__hp - damage_taken) < 0:
            self.__hp = 0
        else:
            self.__hp -= damage_taken
        return damage_taken
    
    def fortify(self):
        self.__fortified = True 
    
    def unfortify(self):
        self.__fortified = False
    
    def heal(self, amount):
        if amount >= 0 and amount <= CivilizationUnit.MAX_HP:
            self.__hp += amount
            if self.__hp > 100:
                self.__hp = 100
            
    def get_hp(self):
        return self.__hp 
            
    def is_fortified(self):
        return self.__fortified
        
    def __str__(self):
        if self.__ranged == True:
            unit_type = "ranged"
        else:
            unit_type = "melee"
        if self.get_hp() == 0:
            status = ", ELIMINATED"
            return "{:s} - {:d}/{:d} ({:s}) - HP: {:d}{:s}".format(self.get_name(), self.__attack_strength, self.__defense_strength, unit_type, self.get_hp(), status)
        elif self.is_fortified():
            status = ", FORTIFIED"
            return "{:s} - {:d}/{:d} ({:s}) - HP: {:d}{:s}".format(self.get_name(), self.__attack_strength, self.__defense_strength, unit_type, self.get_hp(), status)
        else:
            return "{:s} - {:d}/{:d} ({:s}) - HP: {:d}".format(self.get_name(), self.__attack_strength, self.__defense_strength, unit_type, self.get_hp())