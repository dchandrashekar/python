# Bike Stations
# Background
Presently, there are lots of devices equipped with a satellite navigation connection, for instance GPS. This enables them to collect large amounts of location data about the locations and movements of people. For example, it is possible to follow one's movement with activity apps. Many services allow the user to export this data for themselves for future processing. The data used in this exercise has been downloaded from the Sports Tracker service from a personal measurement at Edit -> Export. (Originally .gpx files have been converted to an easier form (csv) with a python script which you can also use to convert your measurements if you are interested.)

# Description
Program uses a file specified by the user to get location and time data. The program uses this data to calculate and tell the user the distance and duration of their journey.

we use files containing information of a single location in each line. The lines are in order and, on average, they have been measured with an interval of 2 seconds. All the files are in the same format, ie. the first 3 lines are metadata (bypassed in this exercise), and other lines contain one location measurement per line. Each measurement contains the details of a location (latitude, longitude and elevation, elevation is not used in this exercise) as well as time (a timestamp in the format (yyyy-MM-ddTHH:mm:ssZ where yyyy is the year, MM the month, dd the day, HH the hour, mm the minute and ss the second. T and Z are constants: T is short for Time and Z means the UTC+0 time.

The lines in the files are in the format

Latitude;Longitude;Elevation;Timestamp

Journey files
example.txt
cycling_herttoniemi_laajasalo.txt
cycling_siilitie_otaniemi.txt
rowing_in_matinkyla.txt
walking_in_aland.txt
Source: GPS data collected with the Sports Tracker mobile app.

program asks the user for a journey file until the user enters a valid file that can be opened. Then the program reads the coordinates and the time data from a file and calculates the distance and duration of the journey and prints them, the distance in kilometers and the duration in minutes.

# Instructions for calculations
Let's examine an example file
30/06/2019 1:12 pm
Sports Tracker
Latitude;Longitude;Elevation;Timestamp
60.10024;19.94226;51.0;2019-06-30T10:12:52Z
60.094845;19.927085;36.0;2019-06-30T10:20:01Z
60.093015;19.92838;32.0;2019-06-30T10:23:24Z
Time / Duration

Because the measurements in the each file are in chronological order, the duration of the journey can be directly calculated from the difference of the timestamps of the first data line (ie. the fourth actual line) and the last line of the file. So, in our example file the time would be between 10:12:52 and 10:23:24, ie. 10 minutes and 32 seconds, ie. about 10.5 minutes. You can assume the timestamps to be within the same day, so you do not need to process the dates in any way. (Of course, it would be advisable to verify the dates in a real application, but to make the exercise easier, we assume the day does not change during the journey. Now our program works fine unless midnight is passed during the journey.)

Distance / length

The total distance of the journey is the sum of the partial distances between all the 2 (consecutive) points. The measurement interval is very short, so we can calculate the partial distances as beeline distances (ie. assuming the journey was straight line between those 2 points). In our example file we would calculate first the distance between the points (60.10024, 19.94226) and (60.094845, 19.927085) (which is about 1.0332 kilometers) and then the distance between the points (60.094845, 19.927085) and (60.093015, 19.92838) (0.2158 kilometers). the total length would be the sum of these, ie. about 1.249 kilometers. In the actual files the points are much closer to each other, and we sum up a large number of small distances. A file containing N lines contains M = N - 3 actual datalines which require M - 1 = N - 3 - 1 distances to be calculated. This means we calculate M - 1 distances between M points. The total distance is the sum of these distances.

We can make the same assumptions as in the citybike exercise: we assume the coordinate system to be rectangular to make the formulas simpler. However, the journys, unlike citybike stations, can be located in any part of the world, so we need to calculate the distance between the longitude degrees, as it depends of the latitude. This is due to the longitudes being the closer to each other the closer we are to the poles. We have been given a ready formula that estimates this based on the average of the two latitudes. (Do not change this formula even if you knwiseew a more exact way to calculate this.) The given numbers latitude_degree_length ja longitude_degree_length repersent the metric distance between 1 latitude degree and 1 longitude degree (in kilometers, in any pat of the world). The latitude degree length is no longer a constant, so both are now written in lowercase for clarity.

The latitudinal and longitudinal distances can be calculated from the respective (angular) coordinate distances by scaling them by the aforementioned multipliers (another of which depends of the latitudes, as mentioned).
The actual distance can be calculated using Pythagorean theorem on the latitudinal and longitudinal distances.
→ The square root can be calculated by raising to the power of (1/2) or alternatively using the sqrt function which you can enable by importing it by writing from math import sqrt at the beginning of your module.


# Execution
Run the bikestation_distances.py file "python bikestation_distances.py"
