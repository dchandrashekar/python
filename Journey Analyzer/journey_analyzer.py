import math
from math import cos, pi
from datetime import datetime
import os

EARTH_RADIUS = 6371 # km
EARTH_CIRCUMFERENCE = EARTH_RADIUS * 2 * pi # km
DEGREE_EQUATOR = EARTH_CIRCUMFERENCE / 360 # km

def degrees_to_radians(deg):
    rad = deg * (pi / 180)
    return rad

def calculate_longitude_degree_length(latitude_degrees):
    return cos(degrees_to_radians(latitude_degrees)) * DEGREE_EQUATOR # km

def calculate_distance(lat1, lon1, lat2, lon2):
    latitude_degree_length = 111.2 # km, roughly constant for our purposes
    longitude_degree_length = calculate_longitude_degree_length( (lat1+lat2) / 2 ) 
    distance = math.sqrt((abs(lon1-lon2)*longitude_degree_length)**2 + (abs(lat1-lat2)*latitude_degree_length)**2)
    return distance

def calculate_time(filename):
    FMT = '%H:%M:%S'
    fileHandle = open (filename, 'r')
    lineList = fileHandle.readlines()
    fileHandle.close()
    time1 = lineList[3].rstrip().replace('T','#').replace('Z','#').split('#')
    time2 = lineList[len(lineList)-1].rstrip().replace('T','#').replace('Z','#').split('#')
    tdelta = datetime.strptime(time2[1], FMT) - datetime.strptime(time1[1], FMT)
    return (tdelta.seconds/60)

def journey_distance(filename):
    fileHandle = open (filename, 'r')
    lineList = fileHandle.readlines()
    fileHandle.close()
    i = 3; j = 4; result = []
    for i in range(len(lineList)-4):
        line1 = lineList[i+3].rstrip().split(";")
        line2 = lineList[j].rstrip().split(";")
        result.append(calculate_distance(float(line1[0]),float(line1[1]),float(line2[0]),float(line2[1])))
        j+=1
    return (sum(result))

def main():
    print("Journey file name:")
    filename = input()
    while os.path.isfile(filename) != True:
        print("Could not open the file {:s}, try again.".format(filename))
        print("Journey file name:")
        filename = input()
    print("Your journey took {:.1f} minutes.".format(calculate_time(filename)))
    print("You travelled {:.1f} kilometers.".format(journey_distance(filename)))

main()
