# Basic Course in Programming Y1
# Author: Dheeraj Chandrashekar
# Class definition for the car_dealer_program

# The class represents the car inventory

class Inventory:

    INITIAL_BALANCE = 50000

    def __init__(self):
        """ 
        Initializes the Inventory
        """
        self.__initial_balance = Inventory.INITIAL_BALANCE
        self.__inventory = {}
    
    def get_balance(self):
        """
        returns the balance available
        """
        return self.__initial_balance
    
    def get_inventory(self):
        """
        returns the inventory values
        """
        return self.__inventory
    
    def delete_record(self, registerid):
        """
        deletes the entry from the inventory for which the received registerid matches
        """
        del self.__inventory[registerid]
    
    def add_balance(self, amount):
        """
        Adds the received amount to the balance
        """
        self.__initial_balance += amount

    def buy_car(self, registerid, description, price):
        """
        This method checks if the given car could be bought and adds it to
        the inventory and thereby deducting the balance
        Returns True if successful else returns False
        """
        if price <= self.get_balance() and registerid not in self.get_inventory():
            self.__inventory[registerid] = [description, price]
            self.__initial_balance -= price
            return True
        else:
            return False
    
    def __str__(self):
        """
        This method displays the current inventory status along with the balance amount
        """
        list1 = []
        string1 = "{:15s} {:20s} {:5s}".format("Register ID", "Description", "Price")
        for id in self.get_inventory():
            list1.append("{:15s} {:20s} {:5d}\n".format(id, self.get_inventory()[id][0], self.get_inventory()[id][1]))
        string2 = "".join(list1)
        string3 = "Current balance is: {:d}".format(self.get_balance())
        return string1 + "\n" + string2 + "\n" + string3


class Car:
    """
    Initializes Car Inventory
    """

    carinventory = Inventory()

    def __init__(self):
        """ 
        Initializes the car object
        """
        self.__offer_list = {}
        self.__cars_sold = []

    def get_offer_list(self):
        """
        returns the offer list 
        """
        return self.__offer_list
    
    def get_cars_sold(self):
        """
        returns the list of cars sold
        """
        return self.__cars_sold

    def offer(self, name, registerid, offer):
        """
        This method registers/stores the offer made for a car
        post checking if its valid
        Returns True if successful else returns False
        """
        list1 = [name, offer]
        if registerid in Car.carinventory.get_inventory() and registerid not in self.get_offer_list():
            self.__offer_list[registerid] = []
        if registerid in Car.carinventory.get_inventory() and registerid in self.get_offer_list():
            self.__offer_list[registerid].append(list1) 
            return True
        else:
            return False

    def sell(self, registerid):
        """
        This methods sells a car for which a highest offer has been made
        post that the sold price is added to the main balance
        The inventory is cleared from the sold car details
        except a record of the sale is stored for history purpose
        Returns True if successful else returns False
        """
        name = []; offer = []
        if registerid in self.get_offer_list():
            for i in range(len(self.get_offer_list()[registerid])):
                name.append(self.get_offer_list()[registerid][i][0])
                offer.append(int(self.get_offer_list()[registerid][i][1]))
            max_offer = max(offer)
            self.__cars_sold.append([registerid, Car.carinventory.get_inventory()[registerid][0], \
                Car.carinventory.get_inventory()[registerid][1], int(max_offer), name[offer.index(max_offer)]])
            Car.carinventory.delete_record(registerid)
            del self.__offer_list[registerid]
            Car.carinventory.add_balance(int(max_offer))
            return True
        else:
            return False
    
    def __str__(self):
        """
        This method displays all the sold car details line by line
        It also prints the total profit/loss gained/incurred by the dealer
        """
        purchase = []; sell = []; list2 = []
        string1 = "{:15s} {:20s} {:15s} {:15s} {:15s}".format("Register ID", "Description", "Purchase price", "Sell price", "Buyer name")
        for i in range(len(self.get_cars_sold())):
            list2.append("{:15s} {:20s} {:<15d} {:<15d} {:15s}\n".format(self.get_cars_sold()[i][0], self.get_cars_sold()[i][1], \
                self.get_cars_sold()[i][2], self.get_cars_sold()[i][3], self.get_cars_sold()[i][4]))
        string2 = "".join(list2)
        for i in range(len(self.get_cars_sold())):
            purchase.append(self.get_cars_sold()[i][2])
            sell.append(self.get_cars_sold()[i][3])
        result = sum(purchase) - sum(sell)
        if result == 0:
            string3 = "You have made NO profit or loss"
        elif result > 0:
            string3 = "You have incurred a loss of: {:d}".format(result)
        else:
            string3 = "You have gained profit of: {:d}".format(abs(result))
        return string1 + "\n" + string2 + "\n" +string3 + "\n"

