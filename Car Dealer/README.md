# Car Dealer
# Description of the exercise
Implement an inventory system for a car dealer that sells used cars. The dealer can buy cars of different kinds that are added to the inventory for sale. The initial balance for the car dealer is 50000 EUR as cash.

The system should implement the following commands:

BUY <register id> "<model description>" <price> : Buys a car with given register number and model desription at a given price, and adds it to the dealer invetory. Cash will be decremented accordingly. Note that model description is surrounded by quotes, because it can contain spaces. Register ID is similar to those used in Finland (e.g. "ABC-123") 

INV : Prints the current inventory along with the following details: register number, model description, purchase price. Finally, the current cash balance should be printed. 

OFFER <name> <register id> <offer> : Register an offer for a car with given register number. This is not yet a sale, but the offer is stored in the system, waiting for other offers. 

SELL <register id> : Sell the given car to the highest offer made for that car. Increase balance accordingly, and remove car from inventory. All offers for the car will be removed from the offers list. 

HISTORY : Prints the sales history of cars sold. It should print the following details: register number, model description, purchase price, sell price, buyer name for each car sold. Finally, the total profit (or loss) should be printed. 

QUIT : exit program

# Execution
Run the car_dealer.py file "python car_dealer.py"
