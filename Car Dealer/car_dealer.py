class Inventory:

    INITIAL_BALANCE = 50000

    def __init__(self):
        """ 
        Initializes the Inventory
        """
        self.__initial_balance = Inventory.INITIAL_BALANCE
        self.__inventory = {}
        self.__offer_list = {}
        self.__cars_sold = []
    
    def get_balance(self):
        return self.__initial_balance
    
    def get_inventory(self):
        return self.__inventory
    
    def get_offer_list(self):
        return self.__offer_list
    
    def get_cars_sold(self):
        return self.__cars_sold
    
    def buy_car(self, registerid, description, price):
        """
        This method checks if the given car could be bought and adds it to
        the inventory and thereby deducting the balance
        Returns True if successful else returns False
        """
        if price <= self.get_balance() and registerid not in self.get_inventory():
            self.__inventory[registerid] = [description, price]
            self.__initial_balance -= price
            return True
        else:
            return False

    def offer(self, name, registerid, offer):
        """
        This method registers/stores the offer made for a car
        post checking if its valid
        Returns True if successful else returns False
        """
        list1 = [name, offer]
        if registerid in self.get_inventory() and registerid not in self.get_offer_list():
            self.__offer_list[registerid] = []
        if registerid in self.get_inventory() and registerid in self.get_offer_list():
            self.__offer_list[registerid].append(list1) 
            return True
        else:
            return False

    def sell(self, registerid):
        """
        This methods sells a car for which a highest offer has been made
        post that the sold price is added to the main balance
        The inventory is cleared from the sold car details
        except a record of the sale is stored for history purpose
        Returns True if successful else returns False
        """
        name = []; offer = []
        if registerid in self.get_offer_list():
            for i in range(len(self.get_offer_list()[registerid])):
                name.append(self.get_offer_list()[registerid][i][0])
                offer.append(int(self.get_offer_list()[registerid][i][1]))
            max_offer = max(offer)
            self.__cars_sold.append([registerid, self.__inventory[registerid][0], self.__inventory[registerid][1], int(max_offer), name[offer.index(max_offer)]])
            del self.__inventory[registerid]
            del self.__offer_list[registerid]
            self.__initial_balance += int(max_offer)
            return True
        else:
            return False

    def history(self):
        """
        This method displays all the sold car details line by line
        It also prints the total profit/loss gained/incurred by the dealer
        """
        purchase = []; sell = []
        print("{:15s} {:20s} {:15s} {:15s} {:15s}".format("Register ID", "Description", "Purchase price", "Sell price", "Buyer name"))
        for i in range(len(self.get_cars_sold())):
            print("{:15s} {:20s} {:<15d} {:<15d} {:15s}".format(self.get_cars_sold()[i][0], self.get_cars_sold()[i][1], self.get_cars_sold()[i][2], self.get_cars_sold()[i][3], self.get_cars_sold()[i][4]))
        print()
        for i in range(len(self.get_cars_sold())):
            purchase.append(self.get_cars_sold()[i][2])
            sell.append(self.get_cars_sold()[i][3])
        result = sum(purchase) - sum(sell)
        if result == 0:
            print("You have made NO profit or loss")
        elif result > 0:
            print("You have incurred a loss of: {:d}".format(result))
        else:
            print("You have gained profit of: {:d}".format(abs(result)))
        print()
    
    def __str__(self):
        """
        This method displays the current inventory status
        """
        print("{:15s} {:20s} {:5s}".format("Register ID", "Description", "Price"))
        for id in self.get_inventory():
             print("{:15s} {:20s} {:5d}".format(id, self.get_inventory()[id][0], self.get_inventory()[id][1]))
        print()
        return "Current balance is: {:d}".format(self.get_balance())
        
        
