def main():
    wordlist = []
    file = open("words.txt", "r")
    line = file.readline()
    while line != "":
        line = line.rstrip()
        wordlist.append(line)
        line = file.readline()
    file.close()
    print("Enter the text to be spell-checked (empty line to end input).")
    lines = []; parts = []; newline = 0; typo_count = 0; res = []; out = []
    while newline != "":
        newline = input()
        if newline:
            lines.append(newline)
    for i in range(len(lines)):
        parts = lines[i].split(" ")
        for j in range(len(parts)):
            if parts[j].lower() in wordlist:
                res.append(parts[j])
            else:
                temp = "*" + parts[j] + "*"
                res.append(temp)
                typo_count += 1
        sentence = ' '.join(res)
        res = []
        out.append(sentence)
    print("Checked text, typos highlighted with '*'")
    for elem in out:
        print(">>", elem)
    if typo_count > 0:
        print("There were {:d} typos.".format(typo_count))
    else:
        print("The text was clean.")

main()
