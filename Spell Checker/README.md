# Spell Checker
# Description
write a spell checker to check the spelling of the input text by the user.
The file words.txt contains a single word per line, all lowercase. A small part of the words contain apostrophes ' but can be handled like any other characters.
The program asks the user for text line by line until the user enters an empty line. You may expect the text to only contain words separeted with spaces. The program scans the text and prints it with the typos highlighted with the asterisk *. The lines of the user's text are marked with >> to distinguish them from the rest of the output. Finally the program prints the number of incorrect words. (or the text The text was clean. in case there weren't any).

# Execution
Run the spell_checker.py file "python spell_checker.py"
