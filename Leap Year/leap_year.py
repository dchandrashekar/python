def main():
    cur_year = int(input("Enter the current year:\n"))
    while cur_year < 0:
        print("Enter a positive year!")
        cur_year = int(input("Enter the current year:\n"))
    fr_year = int(input("Enter the first year to be printed:\n"))
    while fr_year < 0:
        print("Enter a positive year!")
        fr_year = int(input("Enter the first year to be printed:\n"))
    ls_year = int(input("Enter the last year to be printed:\n"))
    while ls_year < 0 or ls_year <= fr_year:
        if ls_year < 0:
            print("Enter a positive year!")
            ls_year = int(input("Enter the last year to be printed:\n"))
        elif ls_year <= fr_year:
            print("Enter a later year than the first!")
            ls_year = int(input("Enter the last year to be printed:\n"))
    print("Leap years from {:4d} to {:4d}:".format(fr_year, ls_year))
    i = 0
    for i in range(fr_year, (ls_year + 1)):
        if ((i % 4 == 0 and i % 100 !=0) or (i % 4 == 0 and i % 400 == 0)) and i < cur_year:
            print(i, "was a leap year")
        if ((i % 4 == 0 and i % 100 !=0) or (i % 4 == 0 and i % 400 == 0)) and i == cur_year:
            print(i, "is a leap year")
        if ((i % 4 == 0 and i % 100 !=0) or (i % 4 == 0 and i % 400 == 0)) and i > cur_year:
            print(i, "will be a leap year")
        
main()
