# Leap Year
# Description
program to print the leap years from an interval specified by the user. The program first asks the user for the current year (the program doesn't check whether it actually is the current year. Afterwards, the program asks for the start year of the interval and then the last year. The program prints every leap year between those two years.

# Execution
Run the leap_year.py file "python leap_year.py"

Note: No errors are handled