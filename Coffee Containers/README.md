# Coffee container
# Description
The program does the following operations on the following order:

Creates the following coffee containers (the jug has a lid, others do not):
a large coffee cup
a small coffee cup
a coffee jug
→ The names and volumes of the containers are aske dfrom the user according to the example run. You may assume that the user enters valid input and that the volume of the large cup is greater than that of the small cup.
Prints the statuse of the three created coffee containers.
Fills the coffee jug (and prints a filling announcement according to the example run).
Prints the status of the coffee jug
Serves (attempts to serve) an user-specified amount from the jug: first to the large cup and then to the small cup. (and prints an annoucement according to the example run).
Prints the amounts of coffee poured in each container.
Prints the statuses of the containers
Checks whether both cups got the same amount of coffee.
If the amount of coffee is equal, the program prints a text telling that the drinkers were content. (ready implemented, the if clause) and stops the program.

If the large cup has more coffee than the small one, continue as follows (the other way around is not possible if we poured the coffee first to the large one):

Prints a text telling that the small coffee cup's holder gets angry (as in the example runs).
"Flips" the small coffee cup.
Prints the status of the small coffee cup.
"Flips" the coffee jug.
Prints the status of the coffee jug
"Force-flips" the coffee jug (ie. first detaches the lid and then flips it).
Prints the status of the coffee jug
Pours as much coffee as possible from the large cup to the small cup (the moment of stealing in the story).
Prints the stolen amount.
Prints the statuses of the coffee cups.
Empties the small coffee cup (the moment of drinking the coffee in the story).
Prints the status of the small coffee cup.

# Execution
Run the coffee_container_program.py file "python coffee_container_program.py"
