# Bike Stations
# Description
Information about csv files
A csv file is a text file where each line consists of fields separeted with commas. (Csv is short for "comma-separated values". Many programs also allow other separators like semicolons.) For instance, an excel file can be saved as a csv file, meaning that values in different columns are written to the file separated with commas or other characters of the user's choice. In engineering calculations it is very common to import and export data between applications in csv files, and programs should be able to read and utilize data from such files.

A csv file is basically the same as a txt file, but it opens by default in a spreadsheet program. The contents of the file can be inspected in raw form by opening it for instance in Notepad or Eclipse. (Right-click the file, choose "Open with" and the desired app. You can also drag and drop the file to the eclipse file view.) The file extension ".csv" can be changed to ".txt" (the file will then open in a text editor) without changing the behaviour of the file in any way.

To open csv files nicely in a text editor, it is recommended to change eclipse settings at Window -> Preferences -> General -> Editors -> File associations -> "Open unassociated files with" -> choose "Text editor".

File	                                         Description
bikestations.csv	                             All city bike stations in Helsinki and Espoo
bikestations_helsinki.csv	                     City bike stations in Helsinki
bikestations_helsinki_errors.csv	             City bike stations in Helsinki (containing invalid coordinates)
bikestations_otaniemi.csv	                     City bike stations in Helsinki in Otaniemi
bikestations_otaniemi_errors.csv	             City bike stations in Helsinki in Otaniemi (containing invalid coordinates)
bikestations_errors_only_1_valid_station.csv	 A file with only 1 valid station

Source: Citybike stations of Helsingin Seudun Liikenne (HSL). Maintained by Helsingin seudun liikenne HSL. Downloaded from Helsinki Region Infoshare on 20.07.2019 with the license Creative Commons Attribution 4.0.

The formatting the files have been changed and unnecessary data hsa been removed to make the exercise easier In the files whose contains "_errors" some of the coordinates have been replaced with text to simulate a situation where numerical data is missing. The Espoo bike station "Haukilahdentie" has been renamed as "Haukilahdentie (Espoo)" in order to not confuse it with the Helsinki station with an identical name. Otherwise the data is original and accurate.


The program reads - from a file of the user's choice - coordinate and name information of citybike stations. They are saved in a dictionary where keys are the names of the stations and the corresponding values are lists (or tuples, if you wish) of length 2 containing in the form [longitude, latitude] An example dictionary with two stations looks as follows:

{    'Kaivopuisto': [24.950292890004903, 60.15544479374228],
 'Laivasillankatu': [24.956347471358754, 60.16095909388713]
}
After reading the file, the program prints the number of valid stations in the form File read succesfully! {n} valid stations found. where {n} is the number of valid stations (which should be equal to the length of the dictionary mentioned above). Valid stations are those whose both coordinates are decimal numbers.

Using this dictionary and the function defined below, the program calculates the shortest and longest distance between two stations in the data.

Finally, the program asks the user for two stations and calculates the distance between them. Both are asked until the user enters a valid station. Valid stations are those found in the file in the 4th column of some line where the coordinates were valid (ie. all keys in the dictionary). For instance in the file bikestations_helsinki_errors.csv The station "Porthania" is valid, but "Designmuseo" is not, because its latitute is not valid. "Herttoniemi" is not valid either, because a station of exactly that name does not exist.

# Execution
Run the bikestation_distances.py file "python bikestation_distances.py"
