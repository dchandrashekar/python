import math
def calculate_station_distance(coordinate_dictionary, station1, station2):
    LONGITUDE_DEGREE_LENGTH = 55.26 # km
    LATITUDE_DEGREE_LENGTH = 111.2 # km
    longi = coordinate_dictionary[station1]
    latt = coordinate_dictionary[station2]
    distance = math.sqrt((abs(float(longi[0])-float(latt[0]))*LONGITUDE_DEGREE_LENGTH)**2 + (abs(float(longi[1])-float(latt[1]))*LATITUDE_DEGREE_LENGTH)**2)
    return distance

def main():
    count = 0; l_dist = 0; s_dist = 100000000.0
    dictionary = {}
    name = input("Enter the filename containing the station data:\n")
    try:
        with open(name, "r") as tempfile:
            next(tempfile)
            for line in tempfile:
                line = line.rstrip()
                parts = line.split(";")
                valid_long = parts[0].replace('.','',1).isdigit()
                valid_latt = parts[1].replace('.','',1).isdigit()
                if valid_long == True and valid_latt == True:
                    dictionary[parts[3]] = (parts[0],parts[1])
                    count += 1
                else:
                    print("Invalid coordinates in line: ", line)
            print("File read succesfully! {:d} valid stations found.".format(count))
            print(" ")
            if count < 2:
                print("Insufficient data for distance calculations.")
            else:
                for val in dictionary:
                    for i in range(len(dictionary)):
                        dist = calculate_station_distance(dictionary, val, list(dictionary.keys())[i])
                        if dist != 0.0 and dist > l_dist and dist != l_dist:
                            l_dist = dist
                            val1 = val
                            val2 = list(dictionary.keys())[i]
                        if dist != 0.0 and dist < s_dist and dist != s_dist:
                            s_dist = dist
                            val3 = val
                            val4 = list(dictionary.keys())[i]
                print("Shortest distance was {:.3f} km between the stations {:s} and {:s}.".format(s_dist, val3, val4))
                print("Longest distance was {:.1f} km between the stations {:s} and {:s}.".format(l_dist,val1,val2))
                print(" ")
                print("Find a distance between any 2 stations!")
                print("First station:")
                station1 = input()
                while station1 not in dictionary:
                    print("Station not found, try again:")
                    station1 = input()
                print("Second station:")
                station2 = input()
                while station2 not in dictionary:
                    print("Station not found, try again:")
                    station2 = input()
                distance = calculate_station_distance(dictionary, station1, station2)
                print("The distance is {:.2f} km between {:s} and {:s}.".format(distance, station1, station2))
    except FileNotFoundError:
        print("Error in reading the file '{:s}'.".format(name))
main()

