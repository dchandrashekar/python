# Friction
# Description
A block in the shape of a rectangular prism has been set on a inclined surface. Whether the block will start to slide on the surface, depends on the static frictional force and the component of gravity parallel to the surface. If the component of gravity parallel to the surface is greater or equal to the maximum value of the static frictional force, the block will start to slide. If the maximum value of the static frictional force is greater, the block will stay in place. Let's assume that the block will not tilt, and let's also assume air resistance to be negligible.

The program first asks the user the angle of inclination of the surface in degrees. Then the program asks the user for the coefficient of friction between the block and the surface. After this the program asks the user for the mass of the block. Afterwards the program calculates the maximum value of the static frictional force and the component of gravity parallel to the surface and prints them. Finally, it tells based on the two forces, whether the block will start to slide down or not.

# Execution
Run the friction.py file "python friction.py"

Note: No errors are handled