import math
G = 9.81

def calculate_maximum_friction(angle_of_incline, coefficient_of_friction, mass):
    angle_in_radians = angle_of_incline * math.pi / 180
    static_frictional_force = mass * G * math.cos(angle_in_radians) * coefficient_of_friction
    return static_frictional_force

def calculate_force_parallel_to_surface(angle_of_incline, mass):
    angle_in_radians = angle_of_incline * math.pi / 180
    force_parallel = mass * G * math.sin(angle_in_radians)
    return force_parallel

def does_the_block_move(friction, parallel_force):
    if parallel_force >= friction:
        return True
    else:
        return False

def main():
    print("A block has been set on a tilted surface.")
    print("The program calculates whether the block will start to slide down.")

    angle_of_incline = float(input("What is the angle of inclination of the surface in degrees (0-45)?\n"))
    while angle_of_incline < 0 or angle_of_incline > 45:
        angle_of_incline = float(input("What is the angle of inclination of the surface in degrees (0-45)?\n"))

    coefficient_of_friction = float(input("What is the coefficient of friction between the block and the surface (0-1)?\n"))
    while coefficient_of_friction < 0 or coefficient_of_friction > 1:
        coefficient_of_friction = float(input("What is the coefficient of friction between the block and the surface (0-1)?\n"))

    mass = float(input("What is the mass of the block (kg)?\n"))
    while mass <= 0:
        mass = float(input("What is the mass of the block (kg)?\n"))

    friction = calculate_maximum_friction(angle_of_incline, coefficient_of_friction, mass)
    parallel_force = calculate_force_parallel_to_surface(angle_of_incline, mass)
    print("The maximum static frictional force is {:.2f} Newtons".format(friction))
    print("The parallel component of the force of gravity is {:.2f} Newtons".format(parallel_force))
    if does_the_block_move(friction, parallel_force):
        print("The block will start to slide down.")
    else:
        print("The block will stay in place.")


main()
    



