# Calorie Calculator
# Background
The energy consumption of physical exercise and other activities cannot be exactly calculated, as it depends on many factors related to both the individual and the environment. However, it can be estimated quite objectively using the MET value (Metabolic Equivalent of Task). The MET value represents the energy consumption relative to he weight of the person and duration of the activity. 1 MET = 1 kcal / kg / hour. For instance:

When mopping lightly ("mopping, standing, light effort", MET value 2.5):
A person weighing 50 kg consumes, in 1 hour, about 50 kg * 1 hour * 2.5 kcal/kg/hour = 125 kcal
A person weighing 100 kg consumes, in 1 hours, about 100 kg * 1 hour * 2.5 kcal/kg/hour = 250 kcal
When shoveling snow ("snow shoveling, by hand, moderate effort", MET value 5.3):
A person weighing 40 kg consumes, in 30 minutes, about 40 kg * 0.5 hours * 5.3 kcal/kg/hour = 106 kcal
A person weighing 40 kg consumes, in 2 hours, about 40 kg * 2 hours * 5.3 kcal/kg/hour = 424 kcal
In a nutshell, the consumption can be calculated according to the formula kcal = activity_duration_h * weigh_kg * activity_met.

# Description
Structure of the file
Take a look at the example file met_values1.txt and this shorter one met_values2.txt containing erroneous lines at the beginning. (You can skip this chapter in case you want to figure the structure of the files out yourself.)

At the beginning of the file there are a few lines of metadata about the source of the data. The actual data contains one activity per line, having its information divided by semicolons ; in the format

[Activity class];[A description of the activity];[MET value]
For example the first actual data lines of the example file met_values1.txt look as follows:
bicycling;bicycling, mountain, uphill, vigorous;14.0
bicycling;bicycling, mountain, competitive, racing;16.0
bicycling;bicycling, BMX;8.5
Valid lines are the ones that
contain 3 or more parts divided by semicolons ;
the 3rd part of which can be converted to a float
Other lines are skipped and the data they contain does not need to be saved and should not be printed to the user. 

program asks the user for the file containing the MET data and attempts to open the file. In case this fails, the program stops after outputting the text Could not open the file '{filename}' where {filename} is the name of the file it tried to open.

If the opening was succesful, it reads the file line by line and saves the activity classes, descriptions of the activities and their corresponding MET values in a suitable data structure. After reading the file the program prints File read successfully. There were {valid_data_lines} valid lines out of {all_lines}. where

{valid_data_lines} is the number of valid lines, ie. such lines that contain at least 3 parts divided by semicolons ; and where the 3rd part can be interpreted as a float.
{all_lines} is the total number of lines.
After this, the program asks the user for their weight and the time spent in the activity. The program first asks the weight repeatedly until the user enters a valid floating-point number in the range between 0 and 500, and then asks the duration of the activity repeatedly until the user enters a valid float in the range between 0 and 1440.

The program then asks the user for a search phrase and uses this to search for all activities (the file always contains 1 activity per line) where the description or the activity class contain this search phrase at some point. For this, you can use the in keyword to search whether a string contains a substring: substring in string. Letter case does not matter, meaning the program should find all activities regardless of whether they contain the keyword in lowercase or uppercase. The program lists these activities in the same order as they were in the file and number them starting from 1. The program asks the user for the number of the activity they meant. This is asked repeately as long as they enter a valid integer in the correct range.

After getting a valid number, the program prints the following information of the corresponding activity:

The chosen activity (so the user can easily see whether they accidentally chose the wrong number)
An estimate of the consumed kilocalories (as presented above)
The text Hope you had a fun time with your activity!

# Execution
Run the calorie_calculator.py file "python calorie_calculator.py"
