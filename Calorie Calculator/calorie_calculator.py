def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def file_read(lineList):
    file_list = []
    for i in range(len(lineList)):
        listi = lineList[i].split(";")
        if len(listi) >= 3 and isfloat(listi[2]) == True:
            file_list.append(listi)
    return (len(file_list), len(lineList))

def main():
    try:
        print("Enter the file name containing the MET data:")
        filename = input()
        fileHandle = open(filename, 'r')
        lineList = fileHandle.readlines()
        fileHandle.close()
        valid_lines, num_of_lines = file_read(lineList)
        print("File read successfully. There were {:d} valid lines out of {:d}.".format(valid_lines, num_of_lines))
        print("Enter your weight in kg:")
        weight = input()
        while isfloat(weight) == False or float(weight) < 0 or float(weight) > 500:
            print("The input must be a number in the range of 0 to 500")
            print("Enter your weight in kg:")
            weight = input()
        print("How long did you exercise in minutes?")
        time = input()
        while isfloat(time) == False or float(time) < 0 or float(time) > 1440:
            print("The input must be a number in the range of 0 to 1440")
            print("How long did you exercise in minutes?")
            time = input()
        print("Enter an activity or word to search for:")
        activity = input()
        filter_file = open("temp.txt", "w")
        for i in range(len(lineList)):
            listi = lineList[i].split(";")
            if len(listi) >= 3 and isfloat(listi[2]) == True:
                filter_file.write(lineList[i])
        filter_file.close()
        filemod = open('temp.txt', 'r')
        newlist = filemod.readlines()
        filemod.close()

        if any(activity.lower() in s for s in newlist):
            print("Your search produced the following results:")
            i = 1
            new_file = open("results.txt", "w")
            for lines in newlist:
                lines = lines.rstrip()
                if activity.lower() in lines.lower():
                    new_file.write(str(i) + "." + lines + "\n")
                    print("{:3d}. {:s}".format(i,lines.split(";")[1]))
                    i += 1
            new_file.close()
            fileHandle = open('results.txt', 'r')
            newlist = fileHandle.readlines()
            fileHandle.close()
            print("-" * 100)
            print("Enter the number of the activity that is the closest to what you meant:")
            num_of_activity = input()
            while isfloat(num_of_activity) == False or float(num_of_activity) < 1 or float(num_of_activity) > len(newlist):
                print("The input must be an integer in the range of 1 to {:d}".format(len(newlist)))
                print("Enter the number of the activity that is the closest to what you meant:")
                num_of_activity = input()
            print("You chose the activity: {:s}".format(newlist[int(num_of_activity) - 1].rstrip().split(";")[1]))
            met_value = float(newlist[int(num_of_activity) - 1].rstrip().split(";")[2])
            calorie = met_value * float(weight) * (float(time)/60)
            print("You consumed approximately {:d} kcal.".format(round(calorie)))
            print("Hope you had a fun time with your activity!")
        else:
            print("None of the activities in the file match your search.")
    except OSError:
        print("Error in reading the file '{:s}'.".format(filename))
main()
